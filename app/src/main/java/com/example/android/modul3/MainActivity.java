package com.example.android.modul3;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    AlertDialog.Builder dialog;
    View dialogView;
    LayoutInflater inflater;
    adapter adapter;
    ArrayList<data> list = new ArrayList<>();
    RecyclerView rview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show(); */
                dialog = new AlertDialog.Builder(MainActivity.this);
                dialogView = getLayoutInflater().inflate(R.layout.customdialog, null);
                dialog.setView(dialogView);
                dialog.setCancelable(true);
                dialog.setTitle("Create new user");
                dialog.setIcon(R.mipmap.ic_launcher);
                final AlertDialog dialogc = dialog.create();

                (dialogView.findViewById(R.id.btnsubmit)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d(MainActivity.class.getSimpleName(), ((Spinner) dialogView.findViewById(R.id.spinner)).getSelectedItem().toString());
                        adapter.addItem(new data(
                                ((EditText) dialogView.findViewById(R.id.edtnama)).getText().toString(),
                                ((EditText) dialogView.findViewById(R.id.edtpekerjaan)).getText().toString(),
                                ((Spinner) dialogView.findViewById(R.id.spinner)).getSelectedItem().toString()

                        ));

                        dialogc.dismiss();
                    }
                });

                (dialogView.findViewById(R.id.btncancel)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogc.dismiss();
                    }
                });

                dialogc.show();


            }
        });

        rview = findViewById(R.id.list);
        rview.setHasFixedSize(true);

        adapter = new adapter(list, new adapter.onclick() {
            @Override
            public void click(data x) {
                Intent intent = new Intent(MainActivity.this, list_item.class);
                intent.putExtra("nama", x.getNama());
                intent.putExtra("pekerjaan", x.getPekerjaan());
                intent.putExtra("jk", x.getJk());
                startActivity(intent);
            }
        });

        rview.setLayoutManager(new LinearLayoutManager(this));
        rview.setAdapter(adapter);

        ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                list.remove(viewHolder.getAdapterPosition());
                adapter.notifyItemRemoved(viewHolder.getAdapterPosition());
            }
        });
        helper.attachToRecyclerView(rview);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            rview.setLayoutManager(new GridLayoutManager(this, 2));
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            rview.setLayoutManager(new LinearLayoutManager(this));
        }
    }
}


