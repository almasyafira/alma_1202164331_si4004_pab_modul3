package com.example.android.modul3;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class adapter extends RecyclerView.Adapter<adapter.viewHolder>{
    ArrayList<data> list = new ArrayList<>();
    adapter.onclick listen;

    interface onclick{
        void click(data x);
    }

    public adapter(ArrayList<data> list, adapter.onclick listener) {
        this.list = list;
        this.listen = listener;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new viewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder viewHolder, final int position) {
        viewHolder.nama.setText(list.get(position).getNama());
        viewHolder.pekerjaan.setText(list.get(position).getPekerjaan());
        if (list.get(position).getJk().equals("Male")){
            viewHolder.imgJK.setImageResource(R.drawable.pria);
        }else if (list.get(position).getJk().equals("Female")){
            viewHolder.imgJK.setImageResource(R.drawable.perempuan);
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { listen.click(list.get(position)); }
        });
    }

    public  void addItem(data br){
        list.add(br);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() { return list.size(); }

    class viewHolder extends RecyclerView.ViewHolder{
        ImageView imgJK;
        TextView nama, pekerjaan;
        public viewHolder(@NonNull View itemView) {
            super(itemView);
            imgJK = itemView.findViewById(R.id.foto1);
            nama = itemView.findViewById(R.id.nama1);
            pekerjaan = itemView.findViewById(R.id.pekerjaan1);
        }
    }
}

