package com.example.android.modul3;

public class data {
    private String nama, pekerjaan, jk;

    public data(String nama, String pekerjaan, String jk) {
        this.nama = nama;
        this.pekerjaan = pekerjaan;
        this.jk = jk;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNama() {
        return nama;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setJk(String jk) {
        this.jk = jk;
    }

    public String getJk() {
        return jk;
    }

}
