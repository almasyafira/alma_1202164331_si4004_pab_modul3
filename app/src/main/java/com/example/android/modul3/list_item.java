package com.example.android.modul3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class list_item extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_item);

        ((TextView)findViewById(R.id.nama_listitem)).setText(getIntent().getStringExtra("nama"));
        ((TextView)findViewById(R.id.pekerjaan_listitem)).setText(getIntent().getStringExtra("pekerjaan"));
        if (getIntent().getStringExtra("jk").equals("Male")){
            ((ImageView)findViewById(R.id.f_listitem)).setImageResource(R.drawable.pria);
        }else {
            ((ImageView)findViewById(R.id.f_listitem)).setImageResource(R.drawable.perempuan);
        }
    }
}
